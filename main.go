package main

import (
	pgs "github.com/lyft/protoc-gen-star"
	pgsgo "github.com/lyft/protoc-gen-star/lang/go"

	"gitlab.com/Pahan84/protoc-gen-jsonfy/module"
)

func main() {
	pgs.Init(pgs.DebugEnv("DEBUG")).
		RegisterModule(module.ASTPrinter(), module.JSONify()).
		RegisterPostProcessor(pgsgo.GoFmt()).
		Render()
}
